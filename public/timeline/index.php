<?php
require_once '../Model/Application.class.php';
require_once '../Model/conf.php';
$movie_url = 'http://api-gocci.jp/api/public/movie/';
$image_url = 'http://api-gocci.jp/api/public/img/';

//concatをせずに、 picture_urlに変える。
$query = "SELECT p.post_id,u.user_id, u.user_name, u.picture, concat(p.movie_url,p.movie) AS movie, res.restname,p.goodnum,comment_num,concat(p.thumbnail_url,p.thumbnail) AS thumbnail,star_evaluation,res.locality
FROM users as u
JOIN posts as p
ON u.user_id = p.user_id
JOIN restaurants as res
ON p.gocci_rest_id = res.id
WHERE permission_id = 1
ORDER BY p.post_id DESC
LIMIT 50";


//var_dump($query);
try {
    $pdo = new PDO("mysql:host={$host}; dbname={$dbname}; charset=utf8;", $user, $password);
        //$pdo = new PDO("mysql:host={$host}; $dbname={$dbname}; charset=utf8;", $user, $password);
	//$rows = $pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
    $rows = $pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
	//var_dump($rows);
	//$pdo = null;
    } catch (PDOException $e) {
        print 'エラー' . $e->getMessage() . "<br>";
        die();
    }
//$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
function unicode_encode($str)
{
  return preg_replace_callback("/\\\\u([0-9a-zA-Z]{4})/", "encode_callback", $str);
}

function encode_callback($matches) {
  return mb_convert_encoding(pack("H*", $matches[1]), "UTF-8", "UTF-16");
}

function json_xencode($value, $options = 0, $unescapee_unicode = true)
{
  $v = json_encode($value, $options);
  if ($unescapee_unicode) {
    $v = unicode_encode($v);
    // スラッシュのエスケープをアンエスケープする
    $v = preg_replace('/\\\\\//', '/', $v);
  }
  return $v;
}
/*
$_SESSION['flag'] = TRUE;
if ($_SESSION['flag'] === TRUE) {
    header('Location: https://codelecture.com/gocci/movie.php');
    exit();
}
*/
session_start();
$json = json_xencode($rows);
@header("Content-Type: text/javascript; charset=utf-8");
print $json;
//session_start();
//if (empty($_SESSION)) {
@$application = unserialize($_SESSION['application']);
//echo $test->getId();
//var_dump($test);

