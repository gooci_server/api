<?php
error_reporting(-1);
ini_set('display_errors', 1);
/*
 * @param array $files
 * @param string $out
 * @param int $delay 切り替えmsec default 50
 * @param int $loop ループ回数, 0で無限ループ default 1
 * @param boolean $autoreverse 自動リバースするかどうか default false
 * @param float $resize_percent リサイズ% default 1.0
 * @param integer $quality 1 が最低, 255が最高 default 128
 */
function animate( $files, $out, $delay = 50, $loop = 1, $autoreverse = false, $resize_percent = 1.0, $quality = 128 ) {
  if ( $autoreverse ) {
    $files = array_merge( $files, array_reverse( array_slice( $files, 1, count($files) - 2 ) ) );
  }
  $anime = new Imagick();
  $anime->setFormat('jpg');
  foreach( $files as $file ) {
    $tmp_gd = imagecreatefromjpeg( $file );
    $w = imagesx( $tmp_gd );
    $h = imagesy( $tmp_gd );
    $n_w = $w * $resize_percent;
    $n_h = $h * $resize_percent;
    $gd = imagecreatetruecolor( $n_w, $n_h );
    imagecopyresized( $gd, $tmp_gd, 0, 0, 0, 0, $n_w, $n_h, $w, $h);
    imagedestroy( $tmp_gd );
    imagetruecolortopalette( $gd, true, $quality );
    $blob = getImageBlob( $gd, 'jpg' );
    $jpg = new Imagick();
    $jpg->readImageBlob( $blob );
    $jpg->setFormat( 'jpg' );
    $jpg->setImageDelay( $delay );
    $jpg->setImageIterations( $loop );
    $anime->addImage( $jpg );
    $jpg->destroy();
  }
  $anime->writeImages($out, true);
  $anime->destroy();
}
 
/**
 * getImageBlob
 */
function getImageBlob( $gd, $type ) {
  ob_start();
  call_user_func( "image{$type}", $gd );
  imagedestroy( $gd );
  $out = ob_get_contents();
  ob_end_clean();
  return $out;
}
/*
$file_name = './movies/test.mp4';
if (file_exists($file_name)) {
//echo 'abc';
$size = filesize($file_name);
//echo $size;
//header("Content-Length: $size");
$cmd = "ffmpeg -ss 0 -t 1 -r 1 -i " . $file_name . "./img/%04d.jpg";
//header("Content-Type: image/jpeg");
//passthru(mb_convert_encoding($cmd, 'SJIS-win', 'UTF-8'));
exec($cmd);
var_dump($cmd);
//echo $cmd;
}
*/
//echo 'a';
//echo 'end';
/*
$files = glob( 'img/*.jpg' );
animate( $files, 'out.jpg', 4, 0, true, 1.0, 256 );
*/
/*
$movie_path = './movies/test.mp4';            // 動画のファイルパス
$movie = new ffmpeg_movie($movie_path);        // インスタンスを生成

$file_name = htmlspecialchars($movie->getFileName(), ENT_QUOTES);    // ファイル名取得
$bitrate = htmlspecialchars($movie->getFrameRate(), ENT_QUOTES);    // フレームレート取得
*/
/*echo "{$file_name}<br />\n";            // ファイル名表示
echo "{$bitrate}<br />\n";            // フレームレート表示
*/
exit();
?>
