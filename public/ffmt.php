<?php
require_once './Model/conf.php';
error_reporting(-1);
ini_set('display_errors', 1);
//変数初期化
$date = date('Y-m-d H:i:s');
//DBと接続
try {
    $pdo = new PDO("mysql:host={$host}; dbname={$dbname}; charset=utf8;",$user,$password);
} catch (PDOException $e) {
    print "error:".$e->getMessage()."<br";
    die();
}
// 生成するサムネイルの幅と高さの最大値
define("LIMIT_WIDTH", 276);
define("LIMIT_HEIGHT", 260);
dl("ffmpeg." . PHP_SHLIB_SUFFIX);    //エクステンションをロード
/*
$updir = './movies/';
$movie = $_FILES["movie"]["name"];
if (is_uploaded_file(@$_FILES["movie"]["tmp_name"])) {
	if (move_uploaded_file($_FILES["movie"]["tmp_name"], $updir.$movie)) {
	    print 'upload ok';
	} else {
	   var_dump("no");
	}
} else {
    var_dump('ファイルを選択してください');
}
*/
//$movie_path = "./movies/abcde.mp4";
//生成するサムネイルの動画のパス
$movie_path = './movies/1e9db8149dab4e12a1fd147f72364313.mp4';
//$movie_path = './movies/' . $movie;
$movie = new ffmpeg_movie($movie_path);    //インスタンス生成

echo $movie->getFileName();  //ファイル名取得、表示
echo "";
echo $movie->getFrameRate();     //フレームレート表示

$frame = $movie->getFrame(100);
$image = $frame->toGDImage();

ob_end_clean();
// HTTP ヘッダ出力
header("Content-type: image/jpeg");
//サムネイル画像の拡張子は.jpgに固定
$extension = ".jpg";
$new_img = md5(uniqid(mt_rand(), true)) . $extension;

//サムネイルをディレクトリに保存する
imageJpeg($image,"./thumbnail/$new_img",100);

//インスタンス解放
imagedestroy($image);

//INSERT処理
$url = "http://api-gocci.jp/gocci/public/thumbnail/";
$stmt = $pdo->prepare("insert into thu (thu,url,date_time) values (:thu,:url,:date_time)");
//$stmt->bindParam(":thu", imageJpeg($image,null,100));
$stmt->bindValue(":thu", $new_img);
$stmt->bindValue(":url", $url);
$stmt->bindValue(":date_time", $date);
$stmt->execute();

?>
