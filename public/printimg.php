<?php
require_once './Model/conf.php';

//DBと接続
try {
    $pdo = new PDO("mysql:host={$host}; dbname={$dbname}; charset=utf8;",$user,$password);
} catch (PDOException $e) {
    print "error:".$e->getMessage()."<br";
    die();
}

$stmt = $pdo->prepare("SELECT thu FROM thu");
$stmt->execute();

$contents_type = array(
    'jpg'  => 'image/jpg',
    'jpeg' => 'image/jpeg',
    'png'  => 'image/png',
    'gif'  => 'image/gif',
    'bmp'  => 'image/bmp',
);

//出力
$img = $stmt->fetchObject();
header("Content-type: image/jpg");
//header('Content-type: ' . $contents_type[$img->thu]);
echo $img->thu;
//var_dump($img);
