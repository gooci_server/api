<?php
require_once './Model/conf.php';

function unicode_encode($str)
{
  return preg_replace_callback("/\\\\u([0-9a-zA-Z]{4})/", "encode_callback", $str);
}

function encode_callback($matches) {
  return mb_convert_encoding(pack("H*", $matches[1]), "UTF-8", "UTF-16");
}

function json_xencode($value, $options = 0, $unescapee_unicode = true)
{
  $v = json_encode($value, $options);
  if ($unescapee_unicode) {
    $v = unicode_encode($v);
    // スラッシュのエスケープをアンエスケープする
    $v = preg_replace('/\\\\\//', '/', $v);
  }
  return $v;
}

//DBと接続
try {
    $pdo = new PDO("mysql:host={$host}; dbname={$dbname}; charset=utf8;",$user,$password);
} catch (PDOException $e) {
    print "error:".$e->getMessage()."<br";
    die();
}

$stmt = $pdo->prepare("SELECT thu FROM thu");
$stmt->execute();
$img = $stmt->fetchObject();
$image =  $img->thu;
//var_dump($image);
$json_box = array('img'=>$image);
$json = json_xencode($json_box);
@header("Content-Type: text/javascript; charset=utf-8");
print $json;
