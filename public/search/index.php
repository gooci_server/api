<?php
//error_reporting(-1);
//ini_set('display_errors', 1);
header('Content-Type: text/html; charset=utf-8');
require_once '../Model/conf.php';
require_once '../Model/Application.class.php';
require_once '../Model/function.php';
//ロジックはindex.phpとほぼ同じ。検索用SQLが追加された程度

$lat=0.001;
$lon=0.001;

if(isset($_GET['lat'])) {
    $lat = $_GET['lat'];
//print("lat=$lat<br>\n");
}
if(isset($_GET['lon'])) {
    $lon = $_GET['lon'];
//print("lon=$lon<br>\n");
}
if(isset($_GET['limit'])) {
    $limit = $_GET['limit'];
//print("limit=$limit<br>\n");
} else {
    $limit = 30;
}
if (isset($_GET['restname'])) {
    $restname = preg_replace('/\A[\s　]+|[\s　]+\z/u','',$_GET['restname']);
    $restname = htmlspecialchars($restname,ENT_QUOTES,'utf-8');
   //$restname = $_GET['restname'];
} else {
    $restname = '';
}
//ひらがなorカタカナの場合の処理

//var_dump($restname);
$query = "SELECT DISTINCT tell, restname, category,lat,lon,restname,locality,FLOOR( SQRT( POWER( ( $lat - lat ) / 0.0111, 2 ) + power( ( $lon - lon ) / 0.0091, 2 ) ) * 1000) AS distance FROM restaurants WHERE restname LIKE " . " '%$restname%' order by distance LIMIT $limit";
//var_dump($query);
$pdo = new PDO("mysql:host={$host}; dbname={$dbname}; charset=utf8;", $user, $password);

if ($restname !== '') {
    @$rows = $pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);
//var_dump($rows);
//print_r($rows);
    $json = json_xencode($rows);
    header("Content-Type: text/javascript; charset=utf-8");
    print $json;
}

