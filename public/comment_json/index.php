<?php
error_reporting(-1);
ini_set('display_errors', 1);
require_once '../Model/function.php';
require '../Model/Application.class.php';
require_once '../Model/conf.php';
header('Content-Type: text/html; charset=utf-8');
//comment一覧をJSONで出力するAPI
session_start();
try {
    $pdo = new PDO("mysql:host={$host}; dbname={$dbname}; charset=utf8;", $user,$password);
    //SELECT処理
} catch (PDOException $e) {
    print 'エラー' . $e->getMessage() . "<br>";
    die();
}
@$post_id = $_GET['post_id'];

$query = "SELECT DISTINCT(comment),u.user_name,u.picture,c.id
FROM posts AS p
JOIN comments AS c
ON p.post_id = c.post_id
JOIN users AS u
ON c.user_id = u.user_id
WHERE p.post_id = '$post_id'
ORDER BY c.id DESC;";

$rows = $pdo->query($query)->fetchAll(PDO::FETCH_ASSOC);

//取得してきた情報をJSON形式で返す
$json = json_xencode($rows);
@header("Content-Type: text/javascript; charset=utf-8");
print $json;

